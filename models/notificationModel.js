var mysqlConn = require('../database/mysql')

var Notification = function(notification) {
    this.type = notification.type,
    this.data = notification.data,
    this.created_at = new Date(),
    this.updated_at = new Date()
}

Notification.Insert =  async function(newNotification, result) {
    try {
        const res = mysqlConn.query ('INSERT INTO notifications set ?', newNotification);
        return res
    } catch (e) {
        console.log('error' + e)
    }
    // mysqlConn.query ('INSERT INTO notifications set ?', newNotification, function (err, res) {
    //     if (err) {
    //         console.log('error:' , err)
    //         result(err, null)
    //     } else {
    //         result(null, res)
    //     }
    // })
}

module.exports = Notification