var express = require('express');
var router = express.Router();
var notifController = require('../controllers/notificationController')

/* GET notification listing. */
router.get('/', notifController.notifGet);

router.post('/', notifController.notifInsert);

router.put('/', notifController.notifGet);
module.exports = router;
