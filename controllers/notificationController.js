var socket = require('socket.io-client')('http://localhost:' + process.env.PORT)
var Notification = require('../models/notificationModel')


exports.notifIndex = function(req, res) {
    res.send('NOT IMPLEMENTED: Site Home Page');
};

exports.notifInsert = async function(req, res) {
    if (!req.headers['x-auth'] && req.headers['x-auth'] === process.env.X_AUTH) return res.sendStatus(401).send({ error: 'Not Found!' })
    var newNotification = new Notification(req.body)
    if (!newNotification.type || !newNotification.data) {
        res.status(400).send({error: true, message: 'Please provide complete data for notification'});
    } else {
        Notification.Insert(newNotification, function(err, notification) {
            if (err)
                res.send(err);
            res.json(notification)
        })
    }
    socket.emit('notif', {message: 'POST notif ok'})
    res.send('POST OK');
};

exports.notifGet = function(req, res) {
    res.send('OK');
    console.log(process.env.X_AUTH)
};